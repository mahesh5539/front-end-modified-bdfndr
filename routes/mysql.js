var mysql = require('mysql');


var p_stack = [];
var queueforwaiting = [];
var poolMaxsize = 1000; 
var starts = 0;
function getConnection(){
	var connection = mysql.createConnection({
		host     : 'localhost',
	    user     : 'root',
	    password : '',
	    database : 'cmpe280lab'
	});
	return connection;
}
for(var i = 0; i < poolMaxsize; i++){
	var conn = getConnection();
	p_stack.push(conn);
}

function getConnectionFromPool(){
	var conn = p_stack.pop();
	return conn;
}

function releaseConnection(conn){
	p_stack.push(conn);
}

function fetchData(callback,queryString){
	
	var connection = getConnectionFromPool();	
		
		conn.query(queryString, function(error, Results){
			
			if(error){				
				console.log("We are in putUser error!!");
				callback(error, Results );
			}
			else
				{
				
				callback(error, Results );
				
				}
	
	});
	}

exports.fetchData=fetchData;