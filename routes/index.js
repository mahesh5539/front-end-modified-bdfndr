var express = require('express');
var router = express.Router();
var Earthquake  = require('./Earthquake');
var Users  = require('./users');
var mysql = require('./mysql');
var fs = require('fs');
/* GET home page. */
router.get('/', function(req, res) {
  res.sendFile('index.html', { root: "C:/Users/Adonis_M/workspace/FrontendCMPE295B/views" });
});
router.get('/getTopMostEarthquakes',Earthquake.getTopMostEarthquakes);
// router.get('/getWalmartData/:city',Earthquake.getWalmartData);
// router.get('/googlemap/locations',Earthquake.getMapLocation);
router.get('/getEarthquakeData',Earthquake.fetchEarthquakeData);
router.get('/writeJSONToFile',Earthquake.writeJSONToFile);

// router.post('/getData/:url',Earthquake.getData);
router.post('/storeEarthquakes/:currentEarthquakeRegion/:currentMag/:latitude/:longitude/:origin_time',Earthquake.storeEarthquakes);
router.get('/getAllEarthquakes',Earthquake.getAllEarthquakes);
router.post('/registerUser/:first_name/:last_name/:email_id',function(req, res) {
	
	var fName = req.params.first_name;
	var lName = req.params.last_name;
	var email = req.params.email_id;
	var queryString = "INSERT INTO useruiweb (firstname, lastname, email)" + " values( \'"+fName+"\',\'"+lName+"\',\'"+email+"\')";
	console.log(queryString);
	mysql.fetchData( function(error, Results){
		if(!error){
			if(Results.affectedRows > 0){
				res.send({"statusIs":"valid"});
			}
			else{
				res.send({"statusIs":"invalid"});
			}		
		}
		else{
			res.send({"user":"failed"});
		}
	},queryString);

	});



router.get('/country', function(req, res) {

	console.log("in");
	fs.readFile('country_iso3166.json', 'utf8', function (err,data) {
	  if (err) {
	    return console.log(err);
	  }
	  res.write(data);
	  res.end();
	  //console.log(data);
	});

	});
	router.get('/clatlng', function(req, res) {

	console.log("in");
	fs.readFile('country_lat_lon.json', 'utf8', function (err,data) {
	  if (err) {
	    return console.log(err);
	  }
	  res.write(data);
	  res.end();
	  //console.log(data);
	});

	});

	router.get('/all', function(req, res) {

	console.log("in");
	fs.readFile('categories/All.json', 'utf8', function (err,data) {
	  if (err) {
	    return console.log(err);
	  }
	  res.write(data);
	  res.end();
	  //console.log(data);
	});

	});

	router.get('/map_indexed', function(req, res) {


	 var img = fs.readFileSync('images/map_indexed.png');
	     res.writeHead(200, {'Content-Type': 'image/gif' });
	     res.end(img, 'binary');

	});

	router.get('/map_outline', function(req, res) {


	 var img = fs.readFileSync('images/map_outline.png');
	     res.writeHead(200, {'Content-Type': 'image/gif' });
	     res.end(img, 'binary');

	});

module.exports = router;
