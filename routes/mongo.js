var request = require('request');


exports.makeWalAPICall = function(callback, finalUrl){
		request(finalUrl, function (error, response, body) {
	   		if(error){
				throw error;
			}	
			else{
				callback(error, response);
			}
		});
};
